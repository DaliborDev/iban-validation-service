<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Service\ValidationServiceInterface;

class ServiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ValidationServiceInterface $validate)
    {
        $this->validate = $validate;
    }

    /**
     * validateIban
     *
     * @param  mixed $iban
     * @return void
     */
    public function validateIban($iban)
    {
       return $this->validate->validateIban($iban);
    }

}
