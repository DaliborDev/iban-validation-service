<?php

namespace App\Service;

/**
 * ValidationService
 */
class ValidationService implements ValidationServiceInterface
{

    /**
     * ibanAllCountryStandards
     *
     * @var array
     */
    public $ibanAllCountryStandards = [];

    /**
     * ibanCountryStandard
     *
     * @var mixed
     */
    private $ibanCountryStandard;

    const STATUS = [
        0 => 'FAILED',
        1 => 'PASS'
    ];

    const RESP_MESSAGES = [
        'FAILED' => "Validation failed",
        'PASS' => "IBAN is valid",
        'UNAVAILABLE' => "Unable to validate IBAN"
    ];

    const MSG = [
        0 => 'The country code of the iban was not found',
        1 => 'The IBAN has invalid lenght or country code',
        2 => 'The IBAN is valid code and has passed all validations',
        3 => 'Check digits validation failed',
        4 => 'Out of my reach `\(";")/`'
    ];

    public function __construct()
    {
        $this->getCountryStandards();
    }

    /**
     * getCountryStandards
     *
     * @return void
     */
    private function getCountryStandards(): void
    {
        //read json file containing country iban standards
        $this->ibanAllCountryStandards = collect(json_decode(file_get_contents(storage_path('app/iban-country-standards.json')), true));
    }

    /**
     * validateIban
     *
     * @param  mixed $iban
     * @return array
     */
    public function validateIban($iban): array
    {
        try {
            $striped = trim(preg_replace('/\s/', '', urldecode($iban)));
            //check country code
            if(!$this->getIbanStandard($striped)) {
                return $this->response(0,0);
            }
            //check iban lenght from country standard
            if(!$this->lenghtValidation($striped)) {
                return $this->response(0,1);
            }
            //validate check digits
            if (!$this->checkDigitValidate($striped)) {
                return $this->response(0,3);
            }
        }
        catch (\Exception $e) {
            return $this->response(0,4);
        }

        return  $this->response(1, 2);
    }

    /**
     * getIbanStandard
     *
     * @param  mixed $iban
     * @return bool
     */
    private function getIbanStandard(string $iban): bool
    {
        $code = substr($iban,0,2);
        $this->ibanCountryStandard = $this->ibanAllCountryStandards->where('country code', $code)->first();
        return count($this->ibanCountryStandard) > 1;
    }

    private function lenghtValidation(string $iban)
    {
        return strlen($iban) == $this->ibanCountryStandard['iban length'];
    }

    /**
     * checkDigitValidate
     *
     * @param  mixed $iban
     * @return bool
     */
    private function checkDigitValidate(string $iban): bool
    {
        //shift first octet to the end
        $ibanArray = str_split($iban, 4);
        $firstOctet = array_shift($ibanArray);
        array_push($ibanArray, $firstOctet);

        //split iban to individual chars to be converted to number
        $charArray = str_split(implode('', $ibanArray));

        //convert all chars to numbers
        $numberIban = [];
        foreach($charArray as $char) {
            if(ctype_alpha($char)) {
                $char = ord($char)-55;
            }
            array_push($numberIban,$char);
        }
        // create the numbered string
        $ibanNumber = implode('',$numberIban);
        $lght = strlen($ibanNumber);
        $cd = '';
        while($lght > 0) {
            if($cd === '') {
                $str = substr($ibanNumber,0,9);
            }
            else {
                $str = substr($ibanNumber,strlen($ibanNumber) - $lght,7);
            }
            $cd = ($cd . $str) % 97;
            $lght = $lght - strlen($str);
        }

        return $cd == 1;
    }

    /**
     * response
     *
     * @param  mixed $status
     * @param  mixed $msg
     * @return array
     */
    private function response(int $status, int $msg): array
    {
        return ['status' => self::RESP_MESSAGES[self::STATUS[$status]], "message" => self::MSG[$msg], 'data' => $this->ibanCountryStandard];
    }

}
