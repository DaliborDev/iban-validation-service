# IBAN Validation service


The IBAN validation service is implemented with micro framework Lumen. All the validation is performed by the ValidationService.php class.
The Validation service loads the IBAN country standards on initialization from file and store it in a local variable as a collection object. The standard is used to validate the country code and the length of each IBAN. 

Check digits are validated using MOD-97-10 algorithm.

Tests can be run with the following command

`vendor/phpunit/phpunit/phpunit tests/ApiTest.php `
