<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ApiTest extends TestCase
{
    /**
     * A positive API test.
     *
     * @return void
     */
    public function testGoodIBANforAllCountries()
    {
        //read json file containing country iban standards
        $ibanCountryStandards = collect(json_decode(file_get_contents(storage_path('app/iban-country-standards.json')), true));
        foreach($ibanCountryStandards as $country) {

            $iban = $country['iban example'];
            if(strlen($iban) < 1) {
                continue;
            }

            $this->get("/api/validate/$iban");

            $this->assertEquals(
                "IBAN is valid", json_decode($this->response->getContent())->status
            );
        }

    }


    /**
     * A negative API test.
     * bad lenght
     * @return void
     */
    public function testBadLenghtIBANforAllCountries()
    {
        //read json file containing country iban standards
        $ibanCountryStandards = collect(json_decode(file_get_contents(storage_path('app/iban-country-standards.json')), true));
        foreach($ibanCountryStandards as $country) {

            $iban = $country['iban example'];
            if(strlen($iban) < 1) {
                continue;
            }

            $this->get("/api/validate/{$iban}1");

            $this->assertEquals(
                'The IBAN has invalid lenght or country code',
                json_decode($this->response->getContent())->message
            );
        }

    }


        /**
     * A negative API test.
     * bad lenght
     * @return void
     */
    public function testFailedCheckDigitsIBAN()
    {
        //read json file containing country iban standards
        $ibanCountryStandards = collect(json_decode(file_get_contents(storage_path('app/iban-country-standards.json')), true));
        foreach($ibanCountryStandards as $country) {

            $iban = $country['iban example'];
            if(strlen($iban) < 1) {
                continue;
            }

            if(strpos($iban, '0')) {

                $iban = str_replace('0', '2',$iban);

                $this->get("/api/validate/$iban");

                $this->assertEquals(
                    'Check digits validation failed',
                    json_decode($this->response->getContent())->message
                );
            }

        }

    }


}
